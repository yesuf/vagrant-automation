I'm a complete newbie so if this ain't possible then who cares?


how can I use version control (git), continuous integration (jenkins), server deployment (vagrant), change management & state management (Saltstack), to build an infrastructure?

I want to deploy servers (base images) with vagrant.
I want to build/configure servers using saltstack
I want to ensure my vagrant files, docker files  and saltstack files are managed through git.
I want to create development space and deploy to production space using jenkins.

I Want to build following servers:

# Production servers -
jenkins server
saltstack master server (one master to rule them all! mwahahahaha....)
saltstack minions with following roles
  - a load balancer
  - a database cluster (two DB servers)
  - multiple web servers (automated transfer of working web app from test env)
  - OMD nagios server (learn to use check_mk and livestatus)

# Test Server -
spin up multiple docker containers for web application development and then when the've been tested migrate the app to web servers that are load balanced in production.




  
